package pod;

import org.jgroups.Message;
import org.jgroups.View;

public class Ejercicio02 extends Ejercicio01 {

	public Ejercicio02(String clusterName) {
		super(clusterName);
	}

	public void viewAccepted(View view) {
		System.out.println("** view: " + view);
	}
	
	@Override
	public void receive(Message msg) {
		System.out.println(msg.getObject());
	}

}
