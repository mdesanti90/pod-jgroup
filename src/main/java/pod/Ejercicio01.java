package pod;

import org.jgroups.JChannel;
import org.jgroups.ReceiverAdapter;


public class Ejercicio01 extends ReceiverAdapter {

	protected JChannel channel;
	private String clusterName;

	public Ejercicio01(String clusterName) {
		try {
			this.clusterName = clusterName;
			this.channel = new JChannel();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void connect() {
		try {
			channel.connect(clusterName);
			channel.setReceiver(this);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void disconnect() {
		channel.disconnect();
		channel.close();
	}
	
}
