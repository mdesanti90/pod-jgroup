package pod;

public class TestEj3 {

	public static void main(String[] args) throws Exception {
		Ejercicio03 ej1 = new Ejercicio03("cluster");
		ej1.connect();
		System.out.println("Envia mensajes");
		ej1.sendMessages("Hola");
		System.out.println("Termina el envio");
		ej1.disconnect();
		System.out.println("Disconnected");
	}
	
}
