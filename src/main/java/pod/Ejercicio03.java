package pod;

import org.jgroups.Address;

public class Ejercicio03 extends Ejercicio02 {

	public Ejercicio03(String clusterName) {
		super(clusterName);
	}
	
	public void sendMessages(String message) throws Exception {
		for(Address addr: channel.getView().getMembers()) {
			channel.send(addr, message);
		}
	}

}
