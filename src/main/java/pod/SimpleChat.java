	package pod;
	
	import org.jgroups.Address;
	import org.jgroups.JChannel;
	import org.jgroups.Message;
	import org.jgroups.ReceiverAdapter;
	
	public class SimpleChat {
	
		private JChannel channel;
		private static final String chatRoomName = "chat";
	
		public void connect(ReceiverAdapter receiverAdapter) {
			try {
				this.channel = new JChannel();
				this.channel.connect(chatRoomName);
				this.channel.setReceiver(receiverAdapter);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	
		public void broadcastMessage(String msg) {
			this.sendMessageTo(null, msg);
		}
	
		public void sendMessageTo(Address addr, String msg) {
			try {
				this.channel.send(new Message(addr, msg));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	
		public void doNotReceiveOwnMessages() {
			channel.setDiscardOwnMessages(true);
		}
	
	}
