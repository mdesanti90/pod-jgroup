package pod;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.jgroups.Address;
import org.jgroups.Message;
import org.jgroups.ReceiverAdapter;
import org.jgroups.View;

public class ChatClient extends ReceiverAdapter {
	
	private SimpleChat chatServer;
	private Set<Address> users;
	
	public static void main(String[] args) throws IOException {
		BufferedReader in=new BufferedReader(new InputStreamReader(System.in));
		ChatClient client = new ChatClient();
		while(!Thread.interrupted()) {
			System.out.print("> "); System.out.flush();

            String line=in.readLine().toLowerCase();

            if(line.startsWith("quit") || line.startsWith("exit")) {
                break;
            }
            
            client.sendMessage(line);

		}
	}
	
	public ChatClient() {
		chatServer = new SimpleChat();
		chatServer.connect(this);
		users = new HashSet<Address>();
	}
	
	@Override
	public void receive(Message msg) {
		System.out.println(msg.getObject());
	}
	
	@Override
	public void viewAccepted(View view) {
		if(users.size() > view.getMembers().size()) {
			searchFallenNode(view.getMembers());
		} else {
			searchNewNode(view.getMembers());
		}
	}
	
	private void searchFallenNode(List<Address> newMembers) {
		Collection<Address> disjunction = CollectionUtils.disjunction(users, newMembers);
		users.removeAll(disjunction);
	}
	
	private void searchNewNode(List<Address> newMembers) {
		Collection<Address> disjunction = CollectionUtils.disjunction(newMembers, users);
		users.addAll(disjunction);
	}
	
	public void sendMessage(String message) {
		chatServer.broadcastMessage(message);
	}

}
